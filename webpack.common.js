const path = require('path')

const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const StyleLintPlugin = require('stylelint-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
  entry: {
    lscss: ['./src/js/lscss.js', './src/sass/lscss.scss']
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, 'src/js'),
        exclude: /node_modules/,
        use: ['babel-loader', 'eslint-loader']
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader']
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader']
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['public/js', 'public/css']),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    }),
    new StyleLintPlugin({
      configFile: './stylelint.config.js',
      files: ['./src/sass/*.scss', './src/sass/*/*.scss'],
      syntax: 'scss'
    })
  ]
}
