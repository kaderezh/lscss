const Toggler = () => {
  const elements = document.querySelectorAll('[data-toggle]')

  for (let i = 0; i < elements.length; i += 1) {
    if (!elements[i].classList.contains('disabled')) {
      if ((elements[i].dataset.toggle).startsWith('#')) {
        elements[i].onclick = (e) => {
          e.preventDefault();
          const target = document.getElementById((elements[i].dataset.toggle).substr(1))

          if (target.classList.contains('modal')) {
            document.getElementsByTagName('body')[0].classList.toggle('modal-open')
          }
          target.classList.toggle('open')
        }
      } else {
        elements[i].onclick = (e) => {
          e.preventDefault();
          if (elements[i].parentNode.classList.contains(elements[i].dataset.toggle)) {
            elements[i].parentNode.classList.toggle('open')
          }
        }
      }
    } else {
      elements[i].onclick = (e) => {
        e.preventDefault();
      }
    }
  }
}

export default Toggler;
