const Dismisser = () => {
  const elements = document.querySelectorAll('[data-dismiss]')

  for (let i = 0; i < elements.length; i += 1) {
    // If no href, then the target is the data-toggle value
    if (elements[i].getAttribute('href')) {
      // WIP
      // elements[i].onclick = () => {
      //   document.getElementById(elements[i].getAttribute('href'))
      // .removeChild(elements[i].parentNode)
      // }
    } else {
      elements[i].onclick = () => {
        if (elements[i].parentNode.classList.contains(elements[i].dataset.dismiss)) {
          elements[i].parentNode.parentNode.removeChild(elements[i].parentNode)
        }
      }
    }
  }
}

export default Dismisser;
