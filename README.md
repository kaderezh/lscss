## Browser support
- Firefox 52+
- Chrome 57+
- IE 10+, Edge 12+
- Safari 10.1+ / iOS Safari 10.3+
- Opera 44+

## Installation
The recommended way is to use npm, through the `npm install @soykje/lscss` command. Then you'll just have to import the files into your project:

#### SASS customizable version...
To customize LSCSS, import the SCSS version: `@import "node_modules/@soykje/lscss/src/sass/lscss";`. You'll have to compile your SCSS file(s) using a tool like PostCSS Autoprefixer (to add browser prefixes).

#### ... or CSS minified version
To use LSCSS as it goes, just import the CSS minified version: `@import "node_modules/@soykje/lscss/public/css/lscss";`

#### JS
`import '@soykje/lscss';`

_You can just include the compiled files (minified) directly into you HTML._

## Development
Just run `npm install` to install the project for development, then:
- `npm start` for development mode
- `npm run build` for production mode
